import React from "react";
import {findAllByRole, render,screen,getRoles} from '@testing-library/react';
import Posts from './Posts';
import mockAxios from 'axios';
import NewPost from '../newposts/NewPost';


// import {rest} from 'msw';
// import {server} from '../../mocks/server'

jest.mock('axios');


const postData=[ {
    "userId": 1,
    "id": 1,
    "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
    "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
  },
  {
    "userId": 1,
    "id": 2,
    "title": "qui est esse",
    "body": "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla"
  },];

const resp={data:postData};

test("fetch and render posts",async ()=>{
mockAxios.get.mockResolvedValue(resp)
render(<Posts/>);
const posts=await screen.findAllByRole("listitem")
expect(posts).toHaveLength(2);
})


test("fetching posts failed",async()=>{
mockAxios.get.mockRejectedValue(new Error('fetch error'))
render(<Posts/>);
const error=  await screen.findByRole("errorItem");
expect(error).toHaveTextContent("something went wrong");

})

test("mocking new post",()=>{
jest.mock('../newposts/NewPost')


})



























// test("fetch and render posts",async ()=>{
// render(<Posts/>)
// const posts= await screen.findAllByRole("listItem")
// expect(posts).toHaveLength(3);
// })



// test("error case",async ()=>{
// server.resetHandlers(
//  rest.get("https://jsonplaceholder.typicode.com/posts",(req,res,ctx)=>{
//      return res(ctx.status(500))

//  })
// )
// render(<Posts/>)
// const error= await screen.findByRole("errorItem")
// expect(error).toHaveTextContent('something went wrong')

// })


