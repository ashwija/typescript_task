import React, { useEffect, useState } from "react";
import axios from 'axios'
import NewPost from "../newposts/NewPost";

const Posts=()=>{
    const [posts,setPosts]=useState([]);
    const [error,setError]=useState(false);

    const setNewPost=(newPost)=>{
        console.log("new post");
        setPosts([...posts,newPost]);

    }

    useEffect(()=>{
        axios.get("https://jsonplaceholder.typicode.com/posts")
        .then(res=>
            {
                setPosts(res.data);
            })
        .catch(err=>{
            setError(true)
        })
        },[])

    return(
        <div>
            {error&&<p role="errorItem">something went wrong</p>}
            <NewPost setPosts={setNewPost}/>
            <h1>Posts</h1>
            <ul>
                {posts.map((post,index)=>
                    <li key={index}>
                        <h3>{post.title}</h3>
                        <p>{post.body}</p>
                    </li>
                )}
            </ul>
        </div>
    )

}

export default Posts;