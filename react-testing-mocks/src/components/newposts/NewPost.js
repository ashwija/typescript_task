import axios from "axios";
import React, { useCallback, useState } from "react";

const NewPost=(props)=>{
    const [addPostVisible,setAddPostVisible]=useState(false);
    const [newPost,setNewPost]=useState({title:'',body:''});
    const handleSubmit=(event)=>{
        console.log("hi");   
        event.preventDefault();
        axios.post("https://jsonplaceholder.typicode.com/posts",newPost)
        .then(res=>{ 
            console.log(res) ;
            setAddPostVisible(false); 
            props.setPosts(res.data); 
         })
         .catch(err=>{
             console.log(err);
         })
        // axios({
		// 	method: 'post',
		// 	url: 'https://jsonplaceholder.typicode.com/posts',
		// 	headers: {
        //         "content-Type":"application/json"
        //     },
		// 	data: newPost})
        // .then(res=>{
        //     console.log(res);
        //     setAddPostVisible(false);   
        //     setNewPost({title:'',body:''})
        //     props.setPosts(res.data);
            

        // })
        // .catch(err=>{
        //     console.log(err);
        // });

    }
    const handleTitle=(event)=>{
        setNewPost({...newPost,title:event.target.value})

    }
    const handleBody=(event)=>{
        setNewPost({...newPost,body:event.target.value})

    }
return(
    <div>
        {!addPostVisible && 
            <button onClick={()=>setAddPostVisible(true)}>Add new Post</button>}
        {addPostVisible &&
            <form onSubmit={handleSubmit}>
                <input type="text" aria-label="title" placeholder="title" value={newPost.title} onChange={handleTitle}/><br/>
                <textarea aria-label="body" placeholder="body" value={newPost.body} onChange={handleBody}/><br/>
                <button type="submit">submit</button>
                <button type="cancel" onClick={()=>setAddPostVisible(false)}>cancel</button>
            </form>
        }
    </div>
)

}

export default NewPost;