import {render,fireEvent,screen, waitFor,cleanup} from '@testing-library/react';
import NewPost from './NewPost';
import mockAxios from 'axios';
import Posts from '../posts/Posts';


jest.mock('axios');

const newPostData= {
    "userId": 1,
    "id": 4,
    "title": "eum et est occaecati",
    "body": "ullam et saepe reiciendis voluptatem adipisci\nsit amet autem assumenda provident rerum culpa\nquis hic commodi nesciunt rem tenetur doloremque ipsam iure\nquis sunt voluptatem rerum illo velit"
  }


test("input title field",()=>{
render(<NewPost/>);
const addButton=screen.getByRole("button");
fireEvent.click(addButton);
const inp=screen.getByRole("textbox",{name:"title"});
fireEvent.change(inp,{target:{value:"testpost"}});
expect(inp.value).toBe("testpost");
});

test("textarea body field",()=>{
    render(<NewPost/>);
    const addButton=screen.getByRole("button");
    fireEvent.click(addButton);
    const inp=screen.getByRole("textbox",{name:"body"});
    fireEvent.change(inp,{target:{value:"testbody"}});
    expect(inp.value).toBe("testbody");
    });

test("click on cancel should hide the form",()=>{
    const {getByText}=render(<NewPost/>);
    const addButton=getByText("Add new Post");
    fireEvent.click(addButton);
    expect(screen.getByRole("textbox",{name:"title"})).toBeInTheDocument();
    expect(screen.getByRole("textbox",{name:"body"})).toBeInTheDocument();
    const cancelButton=getByText("cancel");
    fireEvent.click(cancelButton);
    expect(screen.queryByRole("textbox",{name:"title"})).not.toBeInTheDocument();
    expect(screen.queryByRole("textbox",{name:"body"})).not.toBeInTheDocument();
})


test("form submit for adding new post",async ()=>{
    mockAxios.post.mockResolvedValue({data:newPostData})
    const setPostCallback=jest.fn();
    const{queryByPlaceholderText,getByRole}=render(<NewPost setPosts={setPostCallback}/>)
    //open the form
    const addButton=getByRole("button",{name:"Add new Post"});
    await waitFor(()=> fireEvent.click(addButton));

    const titleEle=queryByPlaceholderText("title");
    const bodyEle=queryByPlaceholderText("body");
    const submitButton=getByRole("button",{name:"submit"})
    expect(titleEle.value).toBe("");
    expect(bodyEle.value).toBe("");
    expect(submitButton).toBeInTheDocument();

    //start typing
    fireEvent.change(titleEle,{target:{value:newPostData.title}});
    fireEvent.change(bodyEle,{target:{value:newPostData.body}});
    //submit the form
    await waitFor (()=>fireEvent.click(submitButton))
    expect(mockAxios.post).toHaveBeenCalledTimes(1);
    //form should be hidden
    expect(titleEle).not.toBeInTheDocument();
    expect(bodyEle).not.toBeInTheDocument();
    expect(setPostCallback).toHaveBeenCalledWith(newPostData);

    //new post is displayed

    // expect(screen.getByText(newPostData.title)).toBeInTheDocument()
    // expect(screen.getByText(/ullam et saepe reiciendis voluptatem adipisci/)).toBeInTheDocument()



})