import {createSlice} from '@reduxjs/toolkit'

const countInitialState:count={
countVal:0
}

const countSlice=createSlice({
name:"count",
initialState:countInitialState,
reducers:{
increment:(state)=>{state.countVal=state.countVal+1},
decrement:(state)=>{state.countVal=state.countVal-1}
}
})

export default countSlice;
