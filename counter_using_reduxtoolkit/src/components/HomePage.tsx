import React from "react"
import Button from "./Button"
import {useDispatch,useSelector} from 'react-redux'
import {decrementActionCreator,incrementActionCreator} from '../actions/index'

const HomePage:React.FC=()=>
{
  const dispatch=useDispatch();
  const count=useSelector((state:storeType)=>state.count.countVal)
    return(
        <div style={{padding:"2em"}}>
            <span style={{marginRight:"1em"}}> {count}</span>
            <Button handleClick={()=>dispatch(incrementActionCreator())} color="green">Increment</Button>
            <Button handleClick={()=>dispatch(decrementActionCreator())}color="orange">Decrement</Button>
            
        </div>
    )
}




export default HomePage;