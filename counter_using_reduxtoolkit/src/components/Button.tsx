import React, { PropsWithChildren } from 'react';
interface ButtonProp
{
    color:string,
    handleClick:()=>void
}
const Button=({color,children,handleClick}:PropsWithChildren<ButtonProp>)=>{

return(
    <button onClick={handleClick} style={{backgroundColor:color,marginRight:"1em"}}>{children}</button>

)


}

export default Button;