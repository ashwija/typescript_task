
import { Reducer } from 'redux';

const defaultState: count = {countVal:0};

export const counterReducer: Reducer<count,incrementCountAction|decrementCountAction > = (state = defaultState,action) => {
  const newState={
    countVal:state.countVal
  }
  switch (action.type) {
    case 'INCREMENT_COUNTER':
      newState.countVal=state.countVal+1;
      return newState;
    case 'DECREMENT_COUNTER':
      newState.countVal=state.countVal-1;
      return newState;
    default:
      return state;
  }
};
