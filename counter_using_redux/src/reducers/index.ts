import { createStore, combineReducers, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';

import { counterReducer} from './CounterReducer';

const logger = createLogger();

export const rootReducer = combineReducers({ count:counterReducer });


export const store = createStore(rootReducer, applyMiddleware(logger));
