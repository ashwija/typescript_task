import React from 'react';
import { connect } from 'react-redux';

import Button from './CounterButton';
import { incrementCount, decrementCount } from '../actions';

const HomePage:React.FC<HomePropType>=({count,incrementCount,decrementCount})=> {
    return (
      <div style={{ textAlign: 'center', marginTop: '40px', fontSize: '2rem' }}>
        <span>{count.countVal}</span>
        <Button handleClick={incrementCount} color={'lightgreen'}>
          Increment
        </Button>
        <Button handleClick={decrementCount} color={'orange'}>
          Decrement
        </Button>
      </div>
    );
  }


const mapStateToProps = (state: storeType) => ({
  count: state.count,
});

export default connect(mapStateToProps, {incrementCount,decrementCount})( HomePage);