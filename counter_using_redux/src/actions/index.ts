

export const incrementCount:incrementCountActionCreator = () => {
  return{
    type:"INCREMENT_COUNTER"
  }
};

export const decrementCount:decrementCountActionCreator = () => {
  return{
    type:"DECREMENT_COUNTER"
  }
};
