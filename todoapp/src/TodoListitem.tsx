import React from "react";
import './TodoListitem.css'
interface TodoListItemProps
{
    todo:Todo
    toggleItem:(selectedTodo:Todo)=>void
    deleteItem:(todoItem:Todo)=>void
}

export const TodoListitem:React.FC<TodoListItemProps>=({todo,toggleItem,deleteItem})=>{
return(
    <div>
        <input type="checkbox" checked={todo.done} onChange={()=>toggleItem(todo)}/> 
        <label className={todo.done?"green":"red"}>
            {todo.name} 
            <button onClick={()=>deleteItem(todo)}>delete</button>
        </label> 
    </div>
)

}