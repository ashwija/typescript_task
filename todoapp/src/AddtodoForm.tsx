import React, { useState } from "react";

interface AddtodoFormProps
{
    addItem:(todoItem:Todo)=>void
    
}

export const AddtodoForm=({addItem}:AddtodoFormProps):JSX.Element=>{
    const[uinput,setUInput]=useState('')
    const handleAdd=()=>{
        addItem({name:uinput,done:false})

    }
return(
<div>
    <input type="text" value={uinput} onChange={(e)=>setUInput(e.target.value)}/>
    <button onClick={handleAdd}>add</button>
</div>

)

}