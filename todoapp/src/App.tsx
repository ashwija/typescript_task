import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import { TodoListitem } from './TodoListitem';
import { AddtodoForm } from './AddtodoForm';
import { HooksExamples } from './HooksExamples';
const initialtodos:Array<Todo>=[
  {name:'read books',done:true},
  {name:'play guitar',done:false},
]


function App() {
  const [todos,setTodos]=useState(initialtodos);

  const toggleTodos=(selectedTodo:Todo)=>{
    let newTodo=todos.map(todo=>{
      if (todo.name===selectedTodo.name){
    return {
     ...todo,
     done:!todo.done
    }
  }
  return todo;
    })
    setTodos(newTodo);
  }
  const addTodo=(todoItem:Todo)=>{
    let newTodo=todos.concat(todoItem);
    setTodos(newTodo);
  }
  const deleteTodo=(todoItem:Todo)=>{
    let newTodo=todos.filter(item=>item.name!==todoItem.name)
    setTodos(newTodo);
  }
  return (
    <div className="App">
      {todos.map(todo=><TodoListitem todo={todo}  toggleItem={toggleTodos} deleteItem={deleteTodo}/>)}
     
      <AddtodoForm addItem={addTodo} />
      {/* <HooksExamples text="ashwija" handleChange={e=>{}}/> */}
    </div>
  );
}

export default App;
