import React, { useRef, useState } from "react";

interface Props
{
    text:string,
    ok?:boolean,
    handleChange?: (event:React.ChangeEvent<HTMLInputElement>)=>void
}




export const HooksExamples:React.FC<Props>=({text,handleChange})=>{
    const [count,setCount]=useState<number>(0)
    const inputRef=useRef<HTMLInputElement>(null)
    return(
<div>
    <input ref={inputRef} type="text" value={text} onChange={handleChange}/>
    <div>{count}</div>
    <button onClick={()=>{setCount(count+1)}} >increment</button>
</div>
    )

}