import logo from './logo.svg';
import './App.css';
import Button from './components/button/button';
import ButtonText from './components/buttontext/buttontext';
import Search from './components/search/search';

function App() {
  return (
    <div className="App">
     
     <Button label="click me"/>
     <br/>
     <ButtonText/>
     <br/>
     <Search/>
    </div>
  );
}

export default App;
