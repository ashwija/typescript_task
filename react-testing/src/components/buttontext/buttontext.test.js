import React from "react";
import {render,fireEvent} from "@testing-library/react";
import ButtonText from '../buttontext/buttontext';

it("button rendered properly",()=>{
const {queryByTitle}=render(<ButtonText/>);
const btn=queryByTitle("dummybutton");
expect(btn).toBeTruthy();
})

it("click event test",()=>{
const{queryByTitle} =render(<ButtonText/>)
const btn=queryByTitle("dummybutton");
expect(btn).toHaveTextContent("click me");
fireEvent.click(btn)
expect(btn).toHaveTextContent("clicked");

})