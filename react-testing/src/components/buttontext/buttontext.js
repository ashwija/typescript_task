import React, { useState } from "react";


const ButtonText=()=>{
    const [value,setValue]=useState('click me')
    const handleClick=()=>{
        setValue("clicked")
    }
    return(
        <button onClick={handleClick} title="dummybutton" >{value}</button>
    )
}

export default ButtonText;