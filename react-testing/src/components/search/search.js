import React, { useState } from "react";


const Search=()=>{
    const [value,setValue]=useState()
    const handleInput=(event)=>{
        setValue(event.target.value)
    }
    return(
        <input type="text" value={value} onChange={handleInput}/>
    )
}

export default Search;