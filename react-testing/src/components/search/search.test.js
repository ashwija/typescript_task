import React from "react";
import {render,fireEvent,screen} from "@testing-library/react";
import Search from './search';


it("searh rendered properly",()=>{
render(<Search/>)
const inp=screen.getByRole("textbox");
expect(inp).toBeTruthy();

})

it("change input",()=>{
render(<Search/>)
const inp=screen.getByRole("textbox");
fireEvent.change(inp,{target:{value:"testval"}})
expect(inp.value).toBe("testval");

})