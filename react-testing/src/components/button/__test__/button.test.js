import React from "react";
import ReactDOM from "react-dom";
import Button from "../../button/button" ;
import {render} from '@testing-library/react';
import renderer from "react-test-renderer";

it("renders button without crashing",()=>{
const div=document.createElement("div");
ReactDOM.render(<Button></Button>,div)
})

it("renders button correctly",()=>{
const {getByTestId}=render(<Button label="click"></Button>)
expect(getByTestId("button")).toHaveTextContent("click")

})

it("renders button correctly",()=>{
    const {getByTestId}=render(<Button label="save"></Button>)
    expect(getByTestId("button")).toHaveTextContent("save")
    
    })

it("snap testing",()=>{
    const tree=renderer.create(<Button label="click"></Button>).toJSON();
    expect(tree).toMatchSnapshot();

})