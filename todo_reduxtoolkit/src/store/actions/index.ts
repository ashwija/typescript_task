
import todoReducer from '../reducers/TodoReducer'

export const {  add:addTodoActionCreator,
                delete:deleteTodoActionCreator,
                toggle:toggleActionCreator}=todoReducer.actions