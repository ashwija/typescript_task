import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import {v1 as uuid} from 'uuid'
    
const todoInitialState:Todo[]=[
{
    id:uuid(),
    desc:"read books",
    done:false
},
{
    id:uuid(),
    desc:"play guitar",
    done:false
}


];

const todoSlice=createSlice({
name:"todo",
initialState:todoInitialState,
reducers:{
add:{
reducer:(state,{payload}:PayloadAction<{id:string,desc:string,done:boolean}>)=>{
    state.push(payload)

},
prepare:({desc}:{desc:string})=>{
 return{ 
        payload:{
        id:uuid(),
        desc:desc,
        done:false}
        }

    }   


    },

delete:(state,{payload}:PayloadAction<{id:string}>)=>{
    const index=state.findIndex(todo=>todo.id===payload.id)
    state.splice(index,1)


},

toggle:(state,{payload}:PayloadAction<{id:string,done:boolean}>)=>{

    const toggleTodo=state.find(todo=>todo.id===payload.id)
    if(toggleTodo)
    toggleTodo.done=!payload.done


}


}


});

export default todoSlice;