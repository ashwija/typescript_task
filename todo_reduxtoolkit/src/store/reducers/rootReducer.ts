import { configureStore } from '@reduxjs/toolkit';
import todoReducer from './TodoReducer';


const rootReducer={todos:todoReducer.reducer}

export default configureStore({reducer:rootReducer});