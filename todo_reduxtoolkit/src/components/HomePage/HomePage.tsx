import React from "react";
import {useSelector } from "react-redux";
import AddTodoForm from "../AddTodoForm/AddTodoForm";
import TodoListItem from "../TodoListItem/TodoListItem";

const HomePage:React.FC=()=>{
    const todos=useSelector((state:StoreType)=>state.todos)

    return(
        <div>
             <AddTodoForm/>
             <br/>
            {todos.map((todo:Todo)=> <TodoListItem todo={todo}/>)}
           
            
        </div>

    )

}

export default HomePage;