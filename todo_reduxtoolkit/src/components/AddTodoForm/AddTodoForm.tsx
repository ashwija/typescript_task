import React, { useCallback, useState } from "react";
import { useDispatch } from "react-redux";
import {addTodoActionCreator} from '../../store/actions/index'

const AddTodoForm:React.FC=()=>{
    const [todoInput,setTodoInput]=useState<string>('');
    const [error,setError]=useState<string>()
    const dispatch=useDispatch();

    const handleInput=useCallback((event:React.ChangeEvent<HTMLInputElement>)=>{
        setTodoInput(event.target.value);
    },[])

    const handleClick=useCallback(()=>{
        if(!todoInput){
            setError("please enter value")
            return;
        }
        dispatch(addTodoActionCreator({desc:todoInput}))

    },[todoInput,dispatch])

    return(
        <div>
            <p style={{color:"red"}}>{error?error:null}</p>
            <br/>
            <input type="text" value={todoInput} onChange={handleInput}/>
            <button style={{marginLeft:"1em",padding:"5px",backgroundColor:"green",color:"white"}} onClick={handleClick}>add</button>
  
        </div>
    )


}

export default AddTodoForm