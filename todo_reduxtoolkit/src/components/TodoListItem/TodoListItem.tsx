import React, { useCallback } from "react";
import { useDispatch } from "react-redux";
import {deleteTodoActionCreator,toggleActionCreator} from '../../store/actions/index'


const TodoListItem:React.FC<TodoListItemProps>=({todo})=>{
    const dispatch=useDispatch();


    const handleClick=useCallback(()=>{
        dispatch(deleteTodoActionCreator({id:todo.id}));
    },[dispatch,todo.id])

    const handletoggle=useCallback(()=>{
        dispatch(toggleActionCreator({id:todo.id,done:todo.done}))
    
    },[dispatch,todo.done])

    return(
        <div>
            <br/>
            <label style={{textDecoration:todo.done?"line-through":""}}>{todo.desc}</label>
            <button style={{marginLeft:"1em",padding:"5px",backgroundColor:"red",color:"white"}} onClick={handleClick}>delete</button>
            <button style={{marginLeft:"1em",padding:"5px",backgroundColor:"purple",color:"white"}} onClick={handletoggle}>toggle</button>
        </div>

)


}

export default TodoListItem;